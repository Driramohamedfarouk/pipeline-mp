<div align="center">

  <h1>Wordpress application deployment on AKS</h1>
  
  <p>
   This project automates the provisioning of AKS azure cluster using terraform and deploys a wordpress application with MYSQL database using Helm Chart . This process is fully automated using Gitlab CI CD pipeline.
  </p>
  
</div>

<br />

<!-- Table of Contents -->
# Table of Contents

- [About the Project](#star2-about-the-project)
  
  * [Tech Stack](#space_invader-tech-stack)
  * [Features](#dart-features)
- [Getting Started](#toolbox-getting-started)
  * [Prerequisites](#bangbang-prerequisites)
  * [Deployment](#test_tube-running-tests)
- [Conclusion](#handshake-contact)
- [Contact](#handshake-contact)


  

<!-- About the Project -->
## About the Project


<!-- Screenshots -->

<div align="center"> 
  <img src="assets/pipeline.png" alt="screenshot" />
</div>


<!-- TechStack -->

## Tech Stack

<details>
  <summary>IAC</summary>
  <ul>
    <li><a href="https://www.terraform.io/">Terraform</a></li>
  </ul>
</details>

<details>
  <summary>Deployment</summary>
  <ul>
    <li><a href="https://helm.sh/">Helm </a></li>
  </ul>
</details>
<details>
<summary>Developement</summary>
  <ul>
    <li><a href="https://fr.wordpress.org/">Wordpress</a></li>
  </ul>
</details>
<details>
<summary>Database</summary>
  <ul>
    <li><a href="https://www.mysql.com/">MySQL</a></li>
  </ul>
</details>

<details>
<summary>DevOps</summary>
  <ul>
    <li><a href="https://gitlab.com/">Gitlab CI CD Pipeline</a></li>
    <li><a href="https://www.kasten.io/">Kasten</a></li>
  </ul>
</details>

<!-- Features -->
###  Features

- Deployment of Wordpress & MYSQL database
- Automation of the provisioning of the AKS Cluster using Terraform 
- Automation of the deployment of KASTEN for monitoring and backup purposes



<!-- Getting Started -->
## Getting Started

<!-- Prerequisites -->
### Prerequisites
Before you begin, ensure you have the following prerequisites in place:

1. **Azure Account**: You need an Azure account with the necessary permissions to create an AKS cluster and a service principal.

2. **GitLab Account**: You should have access to a GitLab account and a repository to store your project.

3. **Terraform**: Make sure Terraform is installed on your local machine or (Gitlab Runner). You can download it from [Terraform's official website](https://www.terraform.io/downloads.html).

4. **Azure CLI**: Install the Azure CLI by following the instructions (on Gitlab Runner) [here](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli).

5. **Helm**: Helm is required for managing Kubernetes applications. Install Helm by following the instructions(on Gitlab Runner)[here](https://helm.sh/docs/intro/install/).

6. **GitLab CI/CD Setup**: Make sure you have a GitLab project with a `.gitlab-ci.yml` configured for running your CI/CD pipeline. You can find more information on setting up GitLab CI/CD [here](https://docs.gitlab.com/ee/ci/).

7. **GitLab Runner**: Ensure you have a GitLab Runner configured for your project. You can find installation and setup instructions [here](https://docs.gitlab.com/runner/).

## Deployment

Follow these steps to deploy your AKS cluster and the WordPress application using Helm Charts through GitLab CI/CD:

1. **Clone the Repository**:

   ```shell
   git clone https://gitlab.com/Oumaymaaaa/internship-project
   
   
2. **Create Service Principal for your Gitlab Pipeline to authenticate with AZURE**
      
   ```shell
    az ad sp create-for-rbac --name myServicePrincipalName1 --role reader --scopes /subscriptions/00000000-0000-0000-0000-000000000000/resourceGroups/myRG1

3. **Replace the terraform authentication credentials in the provider.tf file**
   ```shell
     cd  ./Terraform 
     
4. **Create a Storage account and a container to save the remote terraform state**
    ```shell
         az storage account create \
           --name <storage-account> \
           --resource-group <resource-group> \
           --location <location> \
           --sku Standard_ZRS \
           --encryption-services blob
5. **Replace the Backend.tf file with your container information**

     ```shell
         cd Terraform
         
6. **Now that the service Principal is set with the required permissions , the storage account is set, Create Variables in your gitlab project and access them through the Pipeline .gitlab-ci.yaml**
    ```shell
    
          AZURE_CLIENT_ID: $ID
          AZURE_CLIENT_SECRET: $SECRET
          AZURE_SUBSCRIPTION_ID: $SUBSCRIPTION
          AZURE_TENANT_ID: $TENANT
          AZURE_RESOURCE_GROUP: $RG
          AKS_CLUSTER_NAME: $AKSCLUSTER
          
7. **If you have an exitant Resource group that you want to import ,modify the import.tf with your specific information**

8. **Now run your Pipeline and everything will be automatically set into place**
 <div align="center"> 
         <img src="assets/result.png" alt="screenshot" />
      </div>
 
   
9. **The result will look something like this**

  <div align="center"> 
   <img src="assets/aks.png" />
   <img src="assets/application.png" alt="logo" />
    </div>  


## Conclusion

The pipeline orchestrates a seamless deployment process, starting with the provisioning of an Azure Kubernetes Service (AKS) cluster. It then utilizes Helm to deploy the WordPress application, which is accessible through the public IP of the Azure load balancer created during deployment. Additionally, Kasten is deployed to your cluster, providing essential data backup, security, and monitoring capabilities, including Grafana and Prometheus for observability and performance visualization. This automation simplifies the setup of your AKS environment, enhancing application management while ensuring data security and providing comprehensive insights into cluster performance.






